#!/usr/bin/env python
import utils
import os
import glob
import numpy as np
import argparse

parser = argparse.ArgumentParser(
    description='Run this code to convert png images to jpeg and resize also \
    produce .txt files for lmdb.')
parser.add_argument('--images_root', default='./data/images/')
parser.add_argument('--count', default=1)
parser.add_argument('--resize_dim', default='256x256') # call it as 300x300
parser.add_argument('--txt_dir', default='./data/txt/')
parser.add_argument('--ratio', default='48:32:20')
args = parser.parse_args()

def main():
    print "Starting...\n"
    # convert all to jpeg and number them
    # print "Converting to JPEG...\n"
    # utils.number_snapshots(args.images_root, int(args.count))

    # resize all images to square and default 300x300 pixels
    print "Resizing all images..."
    print "Resize Dimension: {0}\n".format(args.resize_dim)
    utils.resize(args.images_root, args.resize_dim)

    # create train.txt val.txt test.txt formats to use in caffe lmdb script
    # print "Converting to txt for LMDB usage...\n"
    # utils.convert2txt(args.images_root, args.txt_dir)

    # shuffle all data and put them back in train/val/test
    # print "Shuffling data and reconstructing train/ test/ val/\n"
    # utils.shuffle_all_data(args.images_root, args.ratio)

    print "Done."

if __name__ == "__main__":
    main()
